#include "Utils.h"

vector<string> &split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

vector<string> split(const string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

string string_format(const string fmt_str, ...) {
	int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
	string str;
	unique_ptr<char[]> formatted;
	va_list ap;
	while (1) {
		formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
		strcpy(&formatted[0], fmt_str.c_str());
		va_start(ap, fmt_str);
		final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
		va_end(ap);
		if (final_n < 0 || final_n >= n)
			n += abs(final_n - n + 1);
		else
			break;
	}
	return std::string(formatted.get());
}

string coordToDouble(string Lat, string Lon) {
	double LatDeg = stoi(Lat.substr(1, 3));
	double LatMin = stoi(Lat.substr(5, 2));
	double LatSec = stoi(Lat.substr(8, 2));
	double LatMSec = stoi(Lat.substr(11, 3));
	double LonDeg = stoi(Lon.substr(1, 3));
	double LonMin = stoi(Lon.substr(5, 2));
	double LonSec = stoi(Lon.substr(8, 2));
	double LonMSec = stoi(Lon.substr(11, 3));
	double ResLat = LatDeg + (LatMin / 60) + (LatSec / 3600) + ((LatMSec / 1000) / 3600);
	double ResLon = LonDeg + (LonMin / 60) + (LonSec / 3600) + ((LonMSec / 1000) / 3600);
	double Res[2];
	Res[0] = ResLat;
	Res[1] = ResLon;
	return to_string(ResLat) + ":" + to_string(ResLon);
}

string GetCoordDistRadFrom(double lat_s, double lon_s, double dist_s, double radial_s)
{
	double lat1 = lat_s * PI / 180.0;
	double lon1 = lon_s * PI / 180.0;
	double lat;
	double lon;
	double radial = (PI / 180.0) * radial_s;
	double dist = (PI / (180.0 * 60.0)) * dist_s;

	lat = asin(sin(lat1)*cos(dist) + cos(lat1)*sin(dist)*cos(radial));
	double dlon = atan2(sin(radial)*sin(dist)*cos(lat1), cos(dist) - sin(lat1)*sin(lat));
	lon = fmod(lon1 - dlon + PI, 2 * PI) - PI;
	/*if (cos(lat) == 0)
		lon = lon1;
	else
		lon = fmodl(lon1 - asin(sin(radial)*sin(dist) / cos(lat)) + PI, 2.0 * PI) - PI;*/
	

	lat = lat * 180.0 / PI;
	lon = lon * 180.0 / PI;
	return to_string(lat) + ":" + to_string(lon);
}