#include "Generator.h"

CGenerator::~CGenerator()
{
	m_outFile.close();
}

void CGenerator::SetTime(int t)
{
	m_time = t;
}

void CGenerator::SetRunway(string r)
{
	m_runway = r;
}

void CGenerator::SetPlanesDeparture(int num)
{
	m_planesDeparture = num;
}

void CGenerator::SetOutputPath(string s)
{
	m_outPath = s;
}

void CGenerator::SetUse36()
{
	m_useR36 = true;
}

void CGenerator::SetUse18()
{
	m_useR36 = false;
}

void CGenerator::SetUseSecondRunway(bool use)
{
	m_useSecondRunway = use;
}

void CGenerator::SetTowerMode(bool use)
{
	m_towerMode = use;
}

void CGenerator::WriteHeader()
{
	fstream headerFile;
	headerFile.open("data/header.txt", fstream::in);
	if (m_outFile.is_open())
	{
		m_outFile << headerFile.rdbuf();
		m_outFile.flush();
		m_outFile << endl << endl;
	}
	headerFile.close();
}

void CGenerator::ParseFP()
{
	fstream flightsFile;
	flightsFile.open("data/flights.txt", fstream::in);
	if (flightsFile.is_open())
	{
		SFlightPlan temp;
		string parsed;
		while (getline(flightsFile, parsed))
		{
			vector <string> splitted = split(parsed, ',');

			temp.aptdep = splitted[1].substr(1);
			temp.aptdest = splitted[2].substr(1);
			temp.speedlevel = splitted[3].substr(1, 9);
			temp.route = splitted[3].substr(11);

			if (temp.aptdep == "UKBB")
				m_listDepartures->push_back(temp);
			else if (temp.aptdest == "UKBB")
				m_listArrivals->push_back(temp);
		}
	}
	flightsFile.close();
}

void CGenerator::WriteGroundAircraft(SFlightPlan fp, string stand, int delay, string ac, string runway)
{
	string cs = GenerateCallsign(fp);
	string alt = split(fp.speedlevel, 'F')[1];
	string spd = fp.speedlevel.substr(1, 3);
	string sid = GetSid(fp.route, runway);

	string standConverted = coordToDouble(m_mapParkings->find(stand)->second.first, m_mapParkings->find(stand)->second.second);

	string toWrite = "PSEUDOPILOT:ALL\n";
	//$FPAUI576:*A:I:B738:420:UKHH:0000:0000:30000:UKBB:00:00:0:0::/v/:KW A137 GOTAP
	toWrite += "@N:" + cs + ":2200:1:" + standConverted + ":437:0:48:0\n$FP" + cs + ":*A:I:" + ac + ":" + spd + ":" + fp.aptdep + ":0000:0000:" + alt + "00:" + fp.aptdest + 
		":00:00:0:0::/v/:" + fp.route + "\n" +
"SIMDATA:" + cs + ":*:*:25:1:0.010\n" +
"$ROUTE:" + sid + fp.route + "\n" +
"IASVARIATION:7\n" +
"DELAY:1:3\n" +
"START:" + to_string(delay) + "\n"
"INITIALPSEUDOPILOT:UKBV_CTR\n\n";

	m_outFile << toWrite;
	m_outFile.flush();
}

void CGenerator::ParseParkings()
{
	fstream parkingsFile;
	parkingsFile.open("data/parkings.txt", fstream::in);
	string parkingString;
	while (getline(parkingsFile, parkingString))
	{
		SParking temp;
		temp.lat = split(parkingString, ':')[0];
		temp.lon = split(parkingString, ':')[1];
		temp.designator = split(parkingString, ':')[2];
		pair<string, string> p;
		p.first = temp.lat;
		p.second = temp.lon;
		m_mapParkings->insert(pair<string, pair<string, string>>(temp.designator, p));
	}

	parkingsFile.close();
}

string CGenerator::GenerateCallsign(SFlightPlan fp)
{
	string company;
	int num = (rand() % 2000) + 1;
	if (num > 1000) num -= 1000;
	
	const string names[] = { "LTU", "EEL", "DLH", "LOT", "WRC", "UKN", "ELY", "BOX", "GEC", "AUI", "BAW", "UDN", "UAR", "IJM", "AFL", "SDM", "SBI", "BRU", "TSO", "WZZ",
	"CSA", "SWR", "SAS", "THY", "NAX", "AUA", "AZA", "AHY", "TUA", "UTN", "BER", "BMI" };

	int p = rand() % 53;
	if (p > 31)
		company = "AUI";
	else
		company = names[p];

	if (num < 10)
		return company + "00" + to_string(num);
	else if (num < 100)
	{
		int p = rand() % 2;
		if (p)
			return company + (to_string(num) + (char)('A' + rand() % 25 + 1));
		return company + "0" + to_string(num);
	}
	return company + to_string(num);
}

void CGenerator::ParseRoutes()
{
	fstream sidFile;
	sidFile.open("data/routes.txt", fstream::in);
	
	string buffer;
	while (getline(sidFile, buffer))
	{
		if (buffer == "")
			continue;
		vector <string> sid = split(buffer, ':');
		STerminalRoute temp;
		temp.ap = sid[1];
		temp.runway = sid[2];
		temp.designator = sid[3];
		temp.fix = sid[3].substr(0, sid[3].length() - 2);
		temp.route = sid[4];
		temp.star = (sid[0] == "SID") ? false : true;

		m_terminalRoutes->push_back(temp);
	}
}

std::string CGenerator::GetSid(string route, string runway)
{
	string fix = split(route, ' ')[0];
	for (auto it = m_terminalRoutes->begin(); it != m_terminalRoutes->end(); it++)
	{
		if ((*it).runway == runway && (*it).fix == fix && (*it).star == false)
		{
			string sid = (*it).route;
			return sid.substr(0, sid.length() - fix.length());
		}
	}
	return "NO";
}

string CGenerator::GetStand(string preferable)
{
	if (preferable == "D")
	{
		int n = rand() % 20 + 1;
		while (true)
		{
			if (m_lockedStands->find(preferable + to_string(n)) == m_lockedStands->end())
			{
				m_lockedStands->insert(preferable + to_string(n));
				return preferable + to_string(n);
			}
			n = rand() % 20 + 1;
		}
	}
	else if (preferable == "M")
	{
		int n = rand() % 31 + 1;
		while (true)
		{
			if (m_lockedStands->find(preferable + to_string(n)) == m_lockedStands->end())
			{
				m_lockedStands->insert(preferable + to_string(n));
				return preferable + to_string(n);
			}
			n = rand() % 31 + 1;
		}
	}
	else if (preferable == "S")
	{
		int n = rand() % 44 + 1;
		while (true)
		{
			if (m_lockedStands->find(preferable + to_string(n)) == m_lockedStands->end())
			{
				m_lockedStands->insert(preferable + to_string(n));
				return preferable + to_string(n);
			}
			n = rand() % 31 + 1;
		}
	}
	return "";
}

string CGenerator::GetACType()
{
	const string actypes[] = { "B738", "B735", "E145", "B737", "A321" };
	return actypes[rand() % 5] + "/Q";
}

string CGenerator::GenerateSquawk()
{
	int squawk = 0;
	while (squawk < 1000 && squawk > 6777)
		squawk = rand() % 7000;

	return to_string(squawk);
}

void CGenerator::WriteAircraft(string lat, string lon, string altitude, string cs, string actype, SFlightPlan fp, string star)
{
	string alt = split(fp.speedlevel, 'F')[1];
	string spd = fp.speedlevel.substr(1, 3);
	string toWrite = "PSEUDOPILOT:ALL\n";
	toWrite += "@N:" + cs + ":" + GenerateSquawk() + ":1:" + lat + ":" + lon + ":" + altitude + ":0:48:0\n$FP" + cs + ":*A:I:" + actype + ":" + spd + ":" + fp.aptdep + ":0000:0000:" + alt + "00:" + fp.aptdest +
		":00:00:0:0::/v/:" + fp.route + "\n" +
		"SIMDATA:" + cs + ":*:*:25:1:0.010\n" +
		"$ROUTE:" + ((m_towerMode)?"":fp.route) + star + "\n" +
		"IASVARIATION:7\n" +
		"DELAY:1:3\n" +
		"START:0" + "\n"
		"INITIALPSEUDOPILOT:UKBV_CTR\n\n";

	m_outFile << toWrite;
	m_outFile.flush();
}

void CGenerator::WriteAirTowerAircraft()
{
	int simTime = m_time * 1.3;

	double prevLat = (m_useR36) ? stod("50.1054406") : 0;
	double prevLon = (m_useR36) ? stod("30.8749372") : 0;

	string star = (m_useR36) ? "BB634/3500 BB630/2970 ILS36R" : "BB434/3500 BB430/2970 ILS18L";

	auto it = m_listArrivals->begin();
	int fpnum = rand() % m_listDepartures->size();
	advance(it, fpnum);
	WriteAircraft(to_string(prevLat), to_string(prevLon), "4000", GenerateCallsign(*it), GetACType(), *it, star);
	m_listArrivals->erase(it);

	double distance = static_cast<double>(simTime) * 7.1 / m_planesArrival; //7100 meters per minute with speed 230 knots

	double distanceNM = distance * 0.539956803;

	if (distanceNM < 6.0)
		distanceNM = 6.0;

	for (int i = 1; i < m_planesArrival; i++)
	{
		auto it = m_listArrivals->begin();
		int fpnum = rand() % m_listDepartures->size();
		advance(it, fpnum);
		
		double newDist = distanceNM + rand() % 3 - 3;
		if (newDist < 6.0)
			newDist = 6.0;
		string newCoord = GetCoordDistRadFrom(prevLat, prevLon, newDist, (m_useR36) ? 176.0 : 356.0);
		double newLat = stod(split(newCoord, ':')[0]);
		double newLon = stod(split(newCoord, ':')[1]);
		WriteAircraft(to_string(newLat), to_string(newLon), "4000", GenerateCallsign(*it), GetACType(), *it, star);
		m_listArrivals->erase(it);
		prevLat = newLat;
		prevLon = newLon;
	}

}

CGenerator::CGenerator()
{
#ifdef DEBUG
	srand(1);
#else // DEBUG
	srand(time(NULL));
#endif
	
	m_listDepartures = new list<SFlightPlan>;
	m_listArrivals = new list<SFlightPlan>;
	m_mapParkings = new map<string, pair<string, string>>;
	m_terminalRoutes = new list<STerminalRoute>;
	m_lockedStands = new set<string>;

	m_outPath = "C:\\sim.txt";
	m_time = 30;
	m_planesDeparture = 20;
	m_planesArrival = 10;
	m_useR36 = true;
	m_towerMode = true;
	m_useSecondRunway = false;

	ParseParkings();
}

//returns not null if error
int CGenerator::Generate()
{
	m_outFile.open(m_outPath, fstream::out);
	if (!m_outFile.is_open())
		return 1;

	ParseFP();
	ParseRoutes();

	WriteHeader();
#ifdef DEBUG
	int simTime = 1;
#else	 // DEBUG
	int simTime = (int)(m_time * 0.8);
#endif
	
	int deltapark = (int)(m_planesDeparture * 0.75);
	if (deltapark > 20)
		deltapark = 20;
	int sierrapark = (int)((m_useSecondRunway) ? m_planesDeparture*0.1 : 0);
	int mikepark = m_planesDeparture - deltapark - sierrapark;
	for (int i = 0; i < deltapark; i++)
	{
		list<SFlightPlan>::iterator it = m_listDepartures->begin();
		int fpnum = rand() % m_listDepartures->size();
		advance(it, fpnum);
		WriteGroundAircraft(*it, GetStand("D"), rand() % simTime, GetACType(), (m_useR36) ? "36R" : "18L");
		m_listDepartures->erase(it);
	}
	for (int i = 0; i < mikepark; i++)
	{
		list<SFlightPlan>::iterator it = m_listDepartures->begin();
		int fpnum = rand() % m_listDepartures->size();
		advance(it, fpnum);
		WriteGroundAircraft(*it, GetStand("M"), rand() % simTime, GetACType(), (m_useR36) ? "36R" : "18L");
		m_listDepartures->erase(it);
	}

	for (int i = 0; i < sierrapark; i++)
	{
		list<SFlightPlan>::iterator it = m_listDepartures->begin();
		int fpnum = rand() % m_listDepartures->size();
		advance(it, fpnum);
		WriteGroundAircraft(*it, GetStand("S"), rand() % simTime, GetACType(), (m_useR36) ? "36L" : "18R");
		m_listDepartures->erase(it);
	}


	//generate air here
	if (m_towerMode)
	{
		WriteAirTowerAircraft();
	}


	m_lockedStands->clear();
	m_terminalRoutes->clear();
	m_listArrivals->clear();
	m_listDepartures->clear();
	m_outFile.close();

	return 0;
}