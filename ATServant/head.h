#pragma once

#include "stdafx.h"
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <stdarg.h>  // For va_start, etc.
#include <memory>    // For std::unique_ptr
#include <cmath>

using namespace std;

#include "Utils.h"
#include "Generator.h"
#include "ATServant.h"
#include "ATServantDlg.h"
#include "Resource.h"