#pragma once

#include "Utils.h"
#include <ctime>
#include <random>

struct SFlightPlan;
struct SParking;
struct STerminalRoute;

class CGenerator
{
public:
	CGenerator();
	~CGenerator();

	int Generate();

	void SetTime(int t);
	void SetRunway(string r);
	void SetPlanesDeparture(int num);
	void SetOutputPath(string s);
	void SetUse36();
	void SetUse18();
	void SetUseSecondRunway(bool use);
	void SetTowerMode(bool use);

private:

	int m_time;
	int m_planesDeparture;
	int m_planesArrival;
	string m_runway;
	string m_outPath;
	bool m_useR36;
	bool m_towerMode;
	bool m_useSecondRunway;

	void WriteHeader();
	void WriteGroundAircraft(SFlightPlan fp, string stand, int delay, string ac, string runway);
	
	void ParseFP();
	void ParseParkings();
	void ParseRoutes();

	string GenerateCallsign(SFlightPlan fp);

	string GetSid(string route, string runway);
	string GetStand(string preferable);
	string GetACType();
	string GenerateSquawk();
	void WriteAircraft(string lat, string lon, string altitude, string cs, string actype, SFlightPlan fp, string star);
	void WriteAirTowerAircraft();
	fstream m_outFile;

	const string defaultATC = "UKBV_CTR";

	list <SFlightPlan> *m_listDepartures;
	list <SFlightPlan> *m_listArrivals;
	map <string, pair <string, string>> *m_mapParkings;
	list <STerminalRoute> *m_terminalRoutes;
	set <string> *m_lockedStands;
};