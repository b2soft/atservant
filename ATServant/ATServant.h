
// ATServant.h : main header file for the PROJECT_NAME application
//

#pragma once

#include "head.h"

// #ifndef __AFXWIN_H__
// 	#error "include 'stdafx.h' before including this file for PCH"
// #endif

#include "resource.h"		// main symbols


// CATServantApp:
// See ATServant.cpp for the implementation of this class
//

class CATServantApp : public CWinApp
{
public:
	CATServantApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CATServantApp theApp;