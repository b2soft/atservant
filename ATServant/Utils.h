#pragma once

#include "head.h"

struct SFlightPlan
{
	string aptdep;
	string aptdest;
	string speedlevel;
	string route;
};

struct SParking 
{
	string lat, lon;
	string designator;
};

struct STerminalRoute
{
	string ap;
	string designator;
	string runway;
	string route;
	string fix;
	bool star = true;
};

vector<std::string> &split(const string &s, char delim, vector<string> &elems);
vector<std::string> split(const string &s, char delim);
string string_format(const string fmt_str, ...);
string coordToDouble(string Lat, string Lon);
string GetCoordDistRadFrom(double lat, double lon, double dist, double radial);

const double PI = 3.141592653589793238463;