
// ATServantDlg.cpp : implementation file
//

#include "ATServantDlg.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CATServantDlg dialog

CATServantDlg::CATServantDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ATSERVANT_DIALOG, pParent)
	, m_editDepNumberText(_T("20"))
	, m_outputPath(_T("C:\\sim.txt"))
	, m_editDurationText(_T("30"))
	, m_btnUseSecondRwyChecked(FALSE)
{
	m_generator = new CGenerator;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CATServantDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NUMDEP, m_editDepNumber);
	DDX_Text(pDX, IDC_NUMDEP, m_editDepNumberText);
	DDV_MaxChars(pDX, m_editDepNumberText, 2);
	DDX_Text(pDX, IDC_OUTPUTEDIT, m_outputPath);
	DDV_MaxChars(pDX, m_outputPath, 255);
	DDX_Text(pDX, IDC_TIME, m_editDurationText);
	DDV_MaxChars(pDX, m_editDurationText, 2);
	DDX_Control(pDX, IDC_TIME, m_editDuration);
	DDX_Control(pDX, IDC_R36, m_radioR36);
	DDX_Control(pDX, IDC_CHECK2, m_btnUseSecondRwy);
	DDX_Check(pDX, IDC_CHECK2, m_btnUseSecondRwyChecked);
}

BEGIN_MESSAGE_MAP(CATServantDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_NUMDEP, &CATServantDlg::OnEnChangeNumdep)
	ON_EN_KILLFOCUS(IDC_NUMDEP, &CATServantDlg::OnEnKillfocusNumdep)
	ON_BN_CLICKED(IDC_OUTPUTBROWSE, &CATServantDlg::OnBnClickedOutputbrowse)
	ON_EN_UPDATE(IDC_OUTPUTEDIT, &CATServantDlg::OnEnUpdateOutputedit)
	ON_EN_CHANGE(IDC_OUTPUTEDIT, &CATServantDlg::OnEnChangeOutputedit)
	ON_EN_KILLFOCUS(IDC_OUTPUTEDIT, &CATServantDlg::OnEnKillfocusOutputedit)
	ON_BN_CLICKED(IDC_GENERATE, &CATServantDlg::OnBnClickedGenerate)
	ON_EN_KILLFOCUS(IDC_TIME, &CATServantDlg::OnEnKillfocusNumdep2)
	ON_BN_CLICKED(IDC_R36, &CATServantDlg::OnBnClickedR36)
	ON_BN_CLICKED(IDC_R18, &CATServantDlg::OnBnClickedR18)
	ON_BN_CLICKED(IDC_CHECK2, &CATServantDlg::OnBnClickedCheck2)
END_MESSAGE_MAP()


// CATServantDlg message handlers

BOOL CATServantDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	m_outputDialog = new CFileDialog(FALSE, L"txt", L"C:\\sim.txt", 6UL, _T("EuroScope simulator file (*.txt)|*.txt|"));
	m_radioR36.SetCheck(1);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CATServantDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CATServantDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CATServantDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CATServantDlg::CheckDepartureBorders()
{
	UpdateData(TRUE);
	int val = _ttoi(m_editDepNumberText);
	if (val > 50 || val < 0)
	{
		m_editDepNumberText = "20";
		UpdateData(FALSE);
		MessageBoxA(this->m_hWnd, "Number of departures must be in range 0 to 50", "ERROR", MB_OK);
		return false;
	}
	return true;
}

bool CATServantDlg::CheckSimulationTime()
{
	UpdateData(TRUE);
	int val = _ttoi(m_editDurationText);
	if (val > 60 || val < 15)
	{
		m_editDurationText = "30";
		UpdateData(FALSE);
		MessageBoxA(this->m_hWnd, "Time of simulation must be in range 15 to 60", "ERROR", MB_OK);
		return false;
	}
	return true;
}

BOOL CATServantDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if ((pMsg->wParam == VK_RETURN) || (pMsg->wParam == VK_ESCAPE))
			pMsg->wParam = VK_TAB;
	}
	return CDialog::PreTranslateMessage(pMsg);
}



void CATServantDlg::OnEnChangeNumdep()
{
	if (CheckDepartureBorders())
		m_generator->SetPlanesDeparture(_ttoi(m_editDepNumberText));
}


void CATServantDlg::OnEnKillfocusNumdep()
{
	if (CheckDepartureBorders())
		m_generator->SetPlanesDeparture(_ttoi(m_editDepNumberText));
}


void CATServantDlg::OnBnClickedOutputbrowse()
{
	int result = m_outputDialog->DoModal();
	if (result == IDOK)
	{
		UpdateData(TRUE);
		m_outputPath = m_outputDialog->GetPathName();
		string t(CW2A(m_outputPath.GetString()));
		m_generator->SetOutputPath(t);
		UpdateData(FALSE);
	}
}


void CATServantDlg::OnEnUpdateOutputedit()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.
	
	// TODO:  Add your control notification handler code here
}


void CATServantDlg::OnEnChangeOutputedit()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	UpdateData(TRUE);
	delete m_outputDialog;
	m_outputDialog = new CFileDialog(FALSE, L"txt", m_outputPath, 6UL, _T("EuroScope simulator file (*.txt)|*.txt|"));
	string t(CW2A(m_outputPath.GetString()));
	m_generator->SetOutputPath(t);
	UpdateData(FALSE);
	// TODO:  Add your control notification handler code here
}


void CATServantDlg::OnEnKillfocusOutputedit()
{
	UpdateData(TRUE);
	delete m_outputDialog;
	m_outputDialog = new CFileDialog(FALSE, L"txt", m_outputPath, 6UL, _T("EuroScope simulator file (*.txt)|*.txt|"));
	string t(CW2A(m_outputPath.GetString()));
	m_generator->SetOutputPath(t);
	UpdateData(FALSE);
}


void CATServantDlg::OnBnClickedGenerate()
{
	int result = m_generator->Generate();
	if (!result)
	{
		MessageBox(L"Generation complete", L"Message", MB_ICONINFORMATION | MB_OK );
	}
}

void CATServantDlg::OnEnKillfocusNumdep2()
{
	if (CheckSimulationTime())
		m_generator->SetTime(_ttoi(m_editDurationText));
}


void CATServantDlg::OnBnClickedR36()
{
	m_generator->SetUse36();
}


void CATServantDlg::OnBnClickedR18()
{
	m_generator->SetUse18();
}


void CATServantDlg::OnBnClickedCheck2()
{
	UpdateData(TRUE);
	m_generator->SetUseSecondRunway(m_btnUseSecondRwyChecked);
	UpdateData(FALSE);
}
