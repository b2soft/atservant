
// ATServantDlg.h : header file
//

#pragma once
#include "Generator.h"
#include "head.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "afxeditbrowsectrl.h"

class CGenerator;

// CATServantDlg dialog
class CATServantDlg : public CDialogEx
{
// Construction
public:
	CATServantDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ATSERVANT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	CGenerator *m_generator;
public:
	bool CheckDepartureBorders();
	bool CheckSimulationTime();
	BOOL PreTranslateMessage(MSG * pMsg);
	// edit for departures number
	CEdit m_editDepNumber;
	// text of departures number
	CString m_editDepNumberText;
	afx_msg void OnEnChangeNumdep();
	afx_msg void OnEnKillfocusNumdep();
	CFileDialog *m_outputDialog;
	afx_msg void OnBnClickedOutputbrowse();
	// outputPath
	CString m_outputPath;
	afx_msg void OnEnUpdateOutputedit();
	afx_msg void OnEnChangeOutputedit();
	afx_msg void OnEnKillfocusOutputedit();
	afx_msg void OnBnClickedGenerate();
	CString m_editDurationText;
	CEdit m_editDuration;
	afx_msg void OnEnKillfocusNumdep2();
	afx_msg void OnBnClickedR36();
	afx_msg void OnBnClickedR18();
	CButton m_radioR36;
	afx_msg void OnBnClickedCheck2();
	CButton m_btnUseSecondRwy;
	BOOL m_btnUseSecondRwyChecked;
};
